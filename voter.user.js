// ==UserScript==
// @name         One Click Voting for Topwebcomics
// @namespace    http://tampermonkey.net/
// @description  On TWC it will automatically clicks on the vote in a voting page. If it is a guest voting page, it will automatically select the proper radio button and send the vote.
// @version      1.0.5
// @author       Las Pinter | https://talesfromsomewhere.com | https://www.deviantart.com/laspinter
// @require      https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js
// @match        *://*.topwebcomics.com/vote/*
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        unsafeWindow
// @run-at       document-end
// @downloadURL     https://bitbucket.org/talesfromsomewhereteam/scripts/raw/master/voter.user.js
// @updateURL       https://bitbucket.org/talesfromsomewhereteam/scripts/raw/master/voter.user.js
// ==/UserScript==

'use strict';
let maxRetry = 5;
let retryInterval = 100;

let waitForElementAndDo = function(selector, callbackWhenPresent, callbackAfterMaxRetry, retries = 0) {
    if (jQuery(selector).length) {
        callbackWhenPresent();
    } else {
        if (retries < maxRetry) {
            setTimeout(function() {
                waitForElementAndDo(selector, callbackWhenPresent, callbackAfterMaxRetry, retries + 1);
            }, retryInterval);
        } else {
            callbackAfterMaxRetry();
        }
    }
};

// Doing a naughty vote
let theNameContainer = "#voteform input[type='hidden'][name='key']";
let voteBtn = "#voteform button";

waitForElementAndDo(theNameContainer, function() {
    let theRadioBtnWithTheName = "#voteform .radio-inline input[value='" + $(theNameContainer).attr("value") + "']";
    waitForElementAndDo(theRadioBtnWithTheName, function() {
        $(theRadioBtnWithTheName).click();
        waitForElementAndDo(voteBtn, function() {
            $(voteBtn).click();
        });
    });
}, function() {
    waitForElementAndDo(voteBtn, function() {
        $(voteBtn).click();
    }, function() {
        window.close();
    });
});